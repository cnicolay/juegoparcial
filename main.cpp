﻿#define GLUT_DISABLE_ATEXIT_HACK
#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include "persona.h"
#include "Music.h"
#include "GL/glut.h"
#include "TextureManager.h"
#define KEY_ESC 27

using namespace std;
using namespace irrklang;
#define GL_BGR 0x80E0
#define GL_BGRA 0x80E1
#define PI 3.14159265359
GLint sprites;
GLint sprites_enemigo;
GLint objeto;
GLint texture;
GLint heroe;
GLint caja;
GLint vida;
GLint niveles;
const int pantalla_width = 600;
const int pantalla_height = 600;
float prop_pantalla = 50;
bool main_vp = 1;
bool secu_vp = 1;

Mapas mm;
Music musica;
Persona p;
Enemigo * enemigos = new Enemigo[20];

float _width = 0.0;
float _height = 0.0;

GLfloat t_a = 0.0;
GLfloat t_d = 0.0;

float v_x = 0;
float v_y = 0;
bool mouse_up = 1;
float mouse_x = 0.0;
float mouse_y = 0.0;
float mouse_angle = 0;

void fun_jugar()
{
    mm.setTexture(texture);
    mm.dibujar_mapa();

    p.set_mapas(mm);
    p.set_sprite(heroe);
    p.set_sprite_nivel(niveles);
    p.set_sprite_salud(vida);
    p.dibujar_persona();

    mm.nivel = p.get_mapa().nivel;
    mm.setMapa(p.get_mapa().get_mapas()[mm.nivel]);

    for(int i =0 ; i < 20  ;++i)
    {
        if(!enemigos[i].muerto){
            enemigos[i].salud_heroe = p.salud;
            enemigos[i].set_mapas(mm);
            enemigos[i].set_sprite(sprites_enemigo);
            enemigos[i].set_pos_heroe(p.getInicio().pos_x,p.getInicio().pos_y);
            enemigos[i].dibujar_enemigo(3,3);
            p.salud = enemigos[i].salud_heroe;
        }
        else {
            mm.setMapa(enemigos[i].get_mapa().get_mapas()[mm.nivel]);
            enemigos[i].set_mapas(p.get_mapa());
        }
        if(enemigos[i].heroe_muerto)
        {
            p.set_llave (0);
            p.set_maletin (0);
            p.set_inicio(0,0);
            p.salud = 30;
            for(int i = 0  ; i < 20 ; ++i )
            {
                enemigos[i].muerto = 0;
            }
            glLoadIdentity();
            enemigos[i].heroe_muerto =0;
        }
    }
    p.malos = enemigos;

}
GLvoid callback_special(int key, int x, int y)
{
    switch (key)
	{
        case GLUT_KEY_UP:

            p.v_y = 1;
            v_y = 1;
            glutPostRedisplay();
            break;
        case GLUT_KEY_DOWN:
             p.v_y = -1;
            v_y = -1;
            glutPostRedisplay();
            break;
        case GLUT_KEY_LEFT:
            p.v_x = -1;
            v_x = -1;
            glutPostRedisplay();
            break;
        case GLUT_KEY_RIGHT:
            p.v_x = 1;
            v_x = 1;
            glutPostRedisplay();
            break;
		case GLUT_KEY_F1:
            heroe = caja;
            break;
	}
}

GLvoid callback_special_up(int key, int x, int y)
{
    switch (key)
	{
        case GLUT_KEY_UP:
			p.contacto = 1;
			p.v_y = 0;
            v_y = 0;
            break;
        case GLUT_KEY_DOWN:
			p.contacto = 1;
            p.v_y = 0;
            v_y = 0;
            break;
        case GLUT_KEY_LEFT:
			p.contacto = 1;
			p.v_x = 0;
            v_x = 0;
            break;
        case GLUT_KEY_RIGHT:
			p.contacto = 1;
			p.v_x = 0;
            v_x = 0;
            break;
		case GLUT_KEY_F1:
            heroe = sprites;
            break;
	}
}
void glPaint(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    //aqui
    fun_jugar();

	glutSwapBuffers();
	glDisable(GL_BLEND);
}

void init_GL(void)
{
	glClearColor(0.4f, 0.4f,0.4f, 0.2f);
	glEnable(GL_TEXTURE_2D);
	musica.play_music();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}

GLvoid window_redraw(GLsizei width, GLsizei height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    _width = abs(-width / prop_pantalla - width / prop_pantalla);
    _height = abs(-height / prop_pantalla - height / prop_pantalla);
    p._width = _width;
    p._height = _height;
    glViewport(0, 0, width, height);
    gluOrtho2D(-width / prop_pantalla, width / prop_pantalla, -height / prop_pantalla, height / prop_pantalla);
	glMatrixMode(GL_MODELVIEW);
}

GLvoid window_key(unsigned char key, int x, int y)
{
	switch (key) {
	case KEY_ESC:
		exit(0);
		break;
	default:
		break;
	}
}

GLvoid window_idle()
{
	glutPostRedisplay();
}

GLvoid callback_motion(int x, int y)
{
	 if(mouse_up)
    {

        mouse_x = x - (pantalla_width / 2);
        mouse_x = (mouse_x * _width) / (pantalla_width / 2);
        mouse_y = (pantalla_height / 2) - y;
        mouse_y = (mouse_y * _height) / (pantalla_height / 2);
        mouse_x = x-300;
        mouse_y = y-300;
        p.mouse_x = mouse_x;
        p.mouse_y = mouse_y;
    }
}

void processMouse(int button , int state, int x, int y)
{
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        p.disparo = 1;
        musica.sonido_disparo();
    }
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
        musica.sonido_disparo();
    if(button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
        musica.cambiar_musica();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(pantalla_width, pantalla_height);
	glutInitWindowPosition(500, 0);
	glutCreateWindow("Trabajo parcial");

	init_GL();

	sprites = TextureManager::Inst()->LoadTexture("imagenes/soldado2.png", GL_BGRA, GL_RGBA);//GL_RGBA_EXT
	sprites_enemigo = TextureManager::Inst()->LoadTexture("imagenes/enemigoj.png", GL_BGRA, GL_RGBA);//GL_RGBA_EXT
	texture = TextureManager::Inst()->LoadTexture("imagenes/fondo.png", GL_BGRA, GL_RGB);//GL_BGR_EXT
	caja = TextureManager::Inst()->LoadTexture("imagenes/caja.png", GL_BGRA, GL_RGB);//GL_BGR_EXT
	vida =  TextureManager::Inst()->LoadTexture("imagenes/vida2.png", GL_BGRA, GL_RGBA);//GL_BGR_EXT
	niveles =  TextureManager::Inst()->LoadTexture("imagenes/nivel.png", GL_BGRA, GL_RGB);//GL_BGR_EXT
    heroe = sprites;
	glutDisplayFunc(glPaint);
	glutReshapeFunc(&window_redraw);
	glutKeyboardFunc(&window_key);
	glutSpecialFunc(&callback_special);
	glutSpecialUpFunc(&callback_special_up);
	glutPassiveMotionFunc(&callback_motion);
	glutMouseFunc(&processMouse);

	glutIdleFunc(&window_idle);

	glutMainLoop();
	return 0;
}
