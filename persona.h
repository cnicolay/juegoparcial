#include "enemigo.h"
#include <cstdio>
#include <time.h>
#include <iostream>
using namespace std;
using namespace irrklang;
#define PI 3.14159265359


class Persona {
    public:
        Persona();
        void dibujar_cuadrado(double ancho, double alto);
        bool preguntar(float ,float);
        bool is_libre(float, float);
        void dibujar_persona();
        void dibujar_adorno();
        void set_inicio(float pos_x_,float pos_y_){inicio.pos_x = pos_x_; inicio.pos_y = pos_y_;}
        void set_sprite(int sprite_){sprites = sprite_;}
        void set_sprite_salud(int sprite_){s_salud= sprite_;}
        void set_sprite_nivel(int sprite_){s_nivel = sprite_;}
        void set_mapas(Mapas m){mm = m;}
        void set_llave(bool sd){llave = sd;}
        void set_maletin(bool sd){maletin = sd;}
        Mapas get_mapa(){return mm;}
        bool get_muerto(){return muerto;}
        Pair getInicio(){return inicio;}
        float calcular_angulo(float x, float y);
        void matar_enemigo(float x, float y);
        float v_x;
        float v_y;
        GLfloat t_d;
        GLfloat t_a;
        int contacto;
        float _width;
        float _height;
        float mouse_x;
        float mouse_y;
        float mouse_angle;
        Enemigo* malos;
        int disparo;
        float salud;
    private:
        int pantalla_width = 600;
        int pantalla_height = 600;
        Pair inicio;
        GLint sprites;
        GLint s_salud;
        GLint s_nivel;
        Mapas mm;
        float pos_sprite;
        bool muerto;
        bool llave;
        bool maletin;
        Music musica;


};
Persona::Persona()
{
    salud = 30;
    inicio.pos_x = 0;
    inicio.pos_y = 0;
    disparo = 0;
    maletin = 0;
    llave = 0;
    v_x = 0;
    v_y = 0;
    contacto = 1;

}
void Persona::matar_enemigo(float x, float y)
{
    if(disparo)
    for(int i = 0 ; i < 20 ; ++i)
    {
        //cout <<malos[i].get_inicio().pos_x<<" "<<malos[i].get_inicio().pos_y<<"    " <<x<<"   "<<y<<endl;
        if(x + 2 >= malos[i].get_inicio().pos_x &&x - 2 <= malos[i].get_inicio().pos_x &&
           y + 2 >= malos[i].get_inicio().pos_y &&y - 2 <= malos[i].get_inicio().pos_y){
            malos[i].muerte();
        }
    }
    disparo = 0;
}
void Persona::dibujar_persona()
{
    if(v_x || v_y)
        glTranslatef(contacto*-v_x * t_d * 10, contacto*-v_y * t_d * 10, 0);
    dibujar_cuadrado(3, 3);
}
void Persona::dibujar_adorno()
{
    //vida
    float factor_x = 0.08333333;
    float factor_y = 0.3333333;
    int vid = (salud- 0.01)/10;

     glBindTexture(GL_TEXTURE_2D, s_salud);
     glBegin(GL_POLYGON);
            glTexCoord2f(0,vid*factor_y);
            glVertex3f(inicio.pos_x -11,inicio.pos_y-11,0);
            glTexCoord2f(3*factor_x,vid*factor_y);
            glVertex3f(inicio.pos_x+6-11,inicio.pos_y-11,0);
            glTexCoord2f(3*factor_x,vid*factor_y+factor_y);
            glVertex3f(inicio.pos_x +6-11,inicio.pos_y+2-11,0);
            glTexCoord2f(0,vid*factor_y+factor_y);
            glVertex3f(inicio.pos_x-11,inicio.pos_y+2-11,0);
    glEnd();
    // maletin
    int maletin_b, llave_b;
    if(maletin)
        maletin_b= 6;
    else
        maletin_b=1;
    if(muerto){
        llave_b = 1;
        maletin_b =1;
    }
    glBindTexture(GL_TEXTURE_2D, s_salud);
    glBegin(GL_POLYGON);
            glTexCoord2f(maletin_b*factor_x,0);
            glVertex3f(inicio.pos_x +8,inicio.pos_y-11,0);
            glTexCoord2f(maletin_b*factor_x +factor_x,0);
            glVertex3f(inicio.pos_x +2 +8,inicio.pos_y-11,0);
            glTexCoord2f(maletin_b*factor_x +factor_x,factor_y);
            glVertex3f(inicio.pos_x +2+ 8,inicio.pos_y+2-11,0);
            glTexCoord2f(maletin_b*factor_x,factor_y);
            glVertex3f(inicio.pos_x +8,inicio.pos_y+2-11,0);
    glEnd();
    //llave
    if(llave)
        llave_b= 3;
    else
        llave_b=1;
    glBindTexture(GL_TEXTURE_2D, s_salud);
    glBegin(GL_POLYGON);
            glTexCoord2f(llave_b*factor_x,0);
            glVertex3f(inicio.pos_x +2,inicio.pos_y-11,0);
            glTexCoord2f(llave_b*factor_x +factor_x,0);
            glVertex3f(inicio.pos_x +2 +2,inicio.pos_y-11,0);
            glTexCoord2f(llave_b*factor_x +factor_x,factor_y);
            glVertex3f(inicio.pos_x +2+ 2,inicio.pos_y+2-11,0);
            glTexCoord2f(llave_b*factor_x,factor_y);
            glVertex3f(inicio.pos_x +2,inicio.pos_y+2-11,0);
    glEnd();
    //nivel
    float factor = 0.11111111111111111;
    int ni = mm.nivel;
    //cout <<s_nivel<<s_salud;
    glBindTexture(GL_TEXTURE_2D, s_nivel);
    glBegin(GL_POLYGON);
            glTexCoord2f(factor*ni,0);
            glVertex3f(inicio.pos_x -11,inicio.pos_y+9,0);
            glTexCoord2f(factor*ni + factor,0);
            glVertex3f(inicio.pos_x+2-11,inicio.pos_y+9,0);
            glTexCoord2f(factor*ni +factor,1);
            glVertex3f(inicio.pos_x +2-11,inicio.pos_y+2+9,0);
            glTexCoord2f(factor*ni,1);
            glVertex3f(inicio.pos_x-11,inicio.pos_y+2+9,0);
    glEnd();
}
void Persona::dibujar_cuadrado(double ancho, double alto)
{
    t_d = glutGet(GLUT_ELAPSED_TIME) / 1000.0 - t_a;
    glPushMatrix();
    if(!is_libre(inicio.pos_x +v_x * t_d * 10,inicio.pos_y + v_y * t_d * 10))
    {
        v_x = 0;
        v_y = 0;
    }
    if(v_x || v_y)
    {
        inicio.pos_x += v_x * t_d * 10;
        inicio.pos_y += v_y * t_d * 10;
    }
    float pos_x_pixel = inicio.pos_x * pantalla_width / _width;
    float pos_y_pixel = inicio.pos_y * pantalla_height / _height;

    mouse_angle = 180 / PI * calcular_angulo(mouse_x, mouse_y);
   // mouse_angle = 180  / PI * atan2(old_mouse_y - pos_y_pixel, old_mouse_x - pos_x_pixel);
   // cout<<mouse_angle<<" "<<pos_x_pixel<<" "<<pos_y_pixel<<" "<<old_mouse_x<<" "<<old_mouse_y<<endl;
   dibujar_adorno();
    glBegin(GL_LINES);
         glVertex3f(inicio.pos_x, inicio.pos_y, 0.0);

         glVertex3f(inicio.pos_x +mouse_x/_width, inicio.pos_y-mouse_y/_height, 0.0);
        //glVertex3f(pos_x_pixel + (mouse_x / _width), pos_y_pixel + (mouse_y / _height), 0.0);
    glEnd();
        matar_enemigo(inicio.pos_x +mouse_x/_width, inicio.pos_y-mouse_y/_height);
    glTranslatef(inicio.pos_x, inicio.pos_y, 0);

    glRotatef(-mouse_angle - 90, 0, 0, 1);

    glBindTexture(GL_TEXTURE_2D, sprites);
    glBegin(GL_POLYGON);
        glTexCoord2f(0,0);
        glVertex3f(-(ancho / 2), -(alto / 2), 0.0);
        glTexCoord2f(1,0);
        glVertex3f(+(ancho / 2), -(alto / 2), 0.0);
        glTexCoord2f(1,1);
        glVertex3f(+(ancho / 2), +(alto / 2), 0.0);
        glTexCoord2f(0,1);
        glVertex3f(-(ancho / 2), +(alto / 2), 0.0);
    glEnd();
    glPopMatrix();
    t_a = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
}
bool Persona::preguntar(float pos_x,float pos_y)
{
    int x_=(pos_x+30)/4;
    int y_ =(pos_y+30)/4;

    char a =mm.get_mapas()[mm.nivel][x_][y_];
    if(a == 'l' || a == 'a' || a == 's'){
       return 1;

    }
    if(a == 'e'){
        if(maletin == 1 && llave == 1){
            for(int i = 0 ; i< 20 ;++i){
                 malos[i].reiniciar();
            }
            mm.nivel=1;
            maletin = 0;
            llave = 0;
        }
       return 1;
    }
    if(a == 'f'){
        if(maletin == 1 && llave == 1){
            for(int i = 0 ; i< 20 ;++i){
                 malos[i].reiniciar();
            }

            mm.nivel=2;
            maletin = 0;
            llave = 0;

        }
       return 1;
    }
    if(a == 'm' && (mm.nivel == 0 || mm.nivel == 1)){
        musica.sonido_objeto();
        maletin = 1;
        mm.setPos(x_,y_,'l');
        return 1;
    }
    if(a == 'b' && mm.nivel == 2){
        musica.sonido_objeto();
        maletin = 1;
        mm.setPos(x_,y_,'s');
        return 1;
    }
    if(a == 'k' && (mm.nivel == 0 || mm.nivel == 1)){
        musica.sonido_objeto();
        llave = 1;
        mm.setPos(x_,y_,'l');
        return 1;
    }
    if(a == 'i' && mm.nivel == 2){
        musica.sonido_objeto();
        llave = 1;
        mm.setPos(x_,y_,'s');
        return 1;
    }
    return 0;
}
bool Persona::is_libre(float pos_x,float pos_y)
{
    if(preguntar(pos_x-1.5,pos_y-1.5) && preguntar(pos_x+1.5,pos_y-1.5) && preguntar(pos_x+1.5,pos_y+1.5) && preguntar(pos_x-1.5,pos_y+1.5))
    {
        return true;
    }
    contacto = 0;
    return false;
}
float Persona::calcular_angulo(float x, float y)
{
    if((x > 0) && (y >= 0))
        return atan(y / x);
    if((x == 0) && (y > 0))
        return PI / 2;
    if(x < 0)
        return (atan(y / x) + PI);
    if((x == 0) && (y < 0))
        return 3 * PI / 2;
    if((x > 0) && (y < 0))
        return (atan(y / x) + 2 * PI);
}
