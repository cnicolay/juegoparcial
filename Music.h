#ifndef MUSIC_H
#define MUSIC_H
#include <irrKlang.h>
#include <iostream>
using namespace irrklang;

/// para que te corra linkea el archivo irrKlang-1.5.0\lib\Win32-gcc\libirrKlang.a
/// ademas en tu search directories agrega la ubicacion del irrKlang-1.5.0\include
/// con eso ya te deberia correr sin problemas

/// toda la musica esta guardada en la carpeta musica

#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll

class Music
{
    public:
        Music()
        {
            tipo_musica = 1;
            engine = irrklang::createIrrKlangDevice();
            if(!engine)
            {
                printf("Could not startup music1\n");
                return;
            }
        }
        void play_music()
        {
            /// el 1er true es para hacerlo que sea un bucle, osea que nunca se detenga
            musica1 = engine->play2D("sonidos/random-encounter-short.wav", true, false, true);
            /// le pongo un volumen mas bajo como es fondo
            musica1->setVolume(0.4);
        }
        void cambiar_musica()
        {
            tipo_musica = !(tipo_musica);
            if(!tipo_musica)
            {
                /// 1ro detengo la musica anterior
                musica1->stop();
                /// luego reprodusco la otra musica
                /// igual el 1er true es para hacerlo que sea un bucle
                musica2 = engine->play2D("sonidos/time-attack-short.wav", true, false, true);
                musica2->setVolume(0.4);
            }
            else
            {
                /// 1ro detengo la musica anterior
                musica2->stop();
                /// luego reprodusco la otra musica
                /// igual el 1er true es para hacerlo que sea un bucle
                musica1 = engine->play2D("sonidos/random-encounter-short.wav", true, false, true);
                musica1->setVolume(0.4);
            }

        }
        void sonido_disparo()
        {
            /// aqui el 1er false es para que se repodusca una sola vez
            tmp = engine->play2D("sonidos/usp.wav", false, false, true);
            tmp->setVolume(0.9);
        }
        void sonido_objeto()
        {
            /// aqui el 1er false es para que se repodusca una sola vez
            tmp = engine->play2D("sonidos/pickup.wav", false, false, true);
            tmp->setVolume(0.9);
        }
        void sonido_caminar()
        {

            tmp = engine->play2D("sonidos/pl_step1.wav", false, false, true);
            tmp->setVolume(0.5);
            std::cout << tmp->getMaxDistance();
            tmp->isFinished();
            //musica2->stop();
        }
        irrklang::ISoundEngine* engine;
        irrklang::ISound* musica1;
        irrklang::ISound* musica2;
        irrklang::ISound* tmp;
        bool tipo_musica;

};

#endif // MUSIC_H
